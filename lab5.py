# -*- coding: utf-8 -*-
"""
McDonald's Problem in PuLP

@author: Kevin Jia
"""

import numpy as np
import pandas as pd # We will discuss this more next week!
from pulp import *

# Pandas DataFrame construction
# Take this as given until next Wednesday
percent = 0.35
MenuItems = ['Hamburger', 'Big Mac', 'Filet-O-Fish', 'McChicken', 'French Fries', 'Coca-Cola']
Energy = pd.Series([910, 2020, 1330, 1530, 1320, 590], index = MenuItems)
Protein = pd.Series([12, 23.7, 15.1, 16.6, 5, 0], index = MenuItems)
Fat = pd.Series([7.9, 26.6, 14.5, 17.1, 16.6, 0], index = MenuItems)
Sodium = pd.Series([525, 686, 587, 893, 304, 33], index = MenuItems)
Cost = pd.Series([2.9, 6.2, 5.9, 6.0, 3.4, 3.2], index = MenuItems)
McDonaldsData = pd.DataFrame({'Energy': Energy, 
                              'Protein': Protein, 
                              'Fat': Fat, 
                              'Sodium': Sodium, 
                              'Cost': Cost})

min_max = [{'Nutrient': 'Sodium', 'Min': 0,'Max': 3220},{'Nutrient': 'Energy', 'Min': 8600,'Max': 8800},{'Nutrient': 'Protein', 'Min': 0,'Max': 55}]

prob = LpProblem('Mcdonalds Problem', LpMinimize)

vars = LpVariable.dicts('Items',McDonaldsData.index,0,None,LpInteger)

prob += lpSum([McDonaldsData.loc[i]['Cost']*vars[i] for i in McDonaldsData.index]), 'Total Cost'


prob += lpSum([McDonaldsData.loc[i]['Sodium']*vars[i] for i in McDonaldsData.index]) <= min_max[0]['Max'], 'Sodium Requirement'
prob += lpSum([McDonaldsData.loc[i]['Energy']*vars[i] for i in McDonaldsData.index]) <= min_max[1]['Max'], 'Energy Upper Requirement'
prob += lpSum([McDonaldsData.loc[i]['Energy']*vars[i] for i in McDonaldsData.index]) >= min_max[1]['Min'], 'Energy Lower Requirement'
prob += lpSum([McDonaldsData.loc[i]['Protein']*vars[i] for i in McDonaldsData.index]) >= min_max[2]['Max'], 'Protein Requirement'
prob += lpSum([37*McDonaldsData.loc[i]['Fat']*vars[i]-percent*McDonaldsData.loc[i]['Energy']*vars[i] for i in McDonaldsData.index]) <= 0, 'Fat 0.35 of Energy Content'

prob.writeLP('McDonaldsProblem.lp')
prob.solve()

print('Status:',LpStatus[prob.status])

for v in prob.variables():
    print(v.name,'=',v.varValue)

print('Total Cost:', value(prob.objective))
# NOT RUN, but to help guide you re: Data Structure here:
# View all items: McDonaldsData
# Get all labels (items): McDonaldsData.index
# Get an entire column: McDonaldsData['Energy']
# Get an entire row: McMcDonaldsData.loc['Hamburger']
# Get information about a particular item: McDonaldsData.loc['Hamburger']['Energy']


